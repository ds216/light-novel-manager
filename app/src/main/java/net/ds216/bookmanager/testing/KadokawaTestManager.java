package net.ds216.bookmanager.testing;

import net.ds216.bookmanager.model.Book;
import net.ds216.bookmanager.util.ISBNUtil;
import net.ds216.bookmanager.util.KadokawaUtil;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Marcus on 10/23/2017.
 */

public class KadokawaTestManager {
	private List<TestObjects> objs;

	public KadokawaTestManager(){
		objs = new ArrayList<>();
		objs.add(new TestObjects("9789864734948","問題兒童的最終考驗(2)"));
		objs.add(new TestObjects("4712961076289","問題兒童的最終考驗(3)失控！精靈列車！（特裝版）"));
		objs.add(new TestObjects("9789864737925","加速世界(21)"));
		objs.add(new TestObjects("9789864739073","煙花"));
		objs.add(new TestObjects("9789862874868","神的記事本(7)"));
		objs.add(new TestObjects("9789862876558","神的記事本(8)"));
		objs.add(new TestObjects("9789862872949","神的記事本(6)"));
		objs.add(new TestObjects("9789862379189","神的記事本(5)"));
		objs.add(new TestObjects("9789862375839","神的記事本(4)"));
		objs.add(new TestObjects("9789861749327","神的記事本(3)"));
		objs.add(new TestObjects("9789862875995","惡魔高校D×D(1)"));

		objs.add(new TestObjects("9789864731992","加速世界(19)"));
		objs.add(new TestObjects("9789864730360","加速世界(18)"));
		objs.add(new TestObjects("9789863259374","加速世界(15)"));
		objs.add(new TestObjects("9789863256991","加速世界(14)"));
		objs.add(new TestObjects("9789863255369","加速世界(13)"));
		objs.add(new TestObjects("9789863252375","加速世界(12)"));
		objs.add(new TestObjects("9789863250234","加速世界(11)"));
		objs.add(new TestObjects("9789864732319","問題兒童的最終考驗(1)"));
		objs.add(new TestObjects("9789863664840","神的記事本(9)（完）"));
		objs.add(new TestObjects("9789864734566","你的名字AnotherSide:Earthbound"));
		objs.add(new TestObjects("9789862875919","問題兒童都來自異世界？(1)"));
		objs.add(new TestObjects("9789863669043","問題兒童都來自異世界?(12)（完）"));
		objs.add(new TestObjects("9789863660347","問題兒童都來自異世界？(9)(普通版)"));
		objs.add(new TestObjects("4712961070393","問題兒童都來自異世界?(12)（完）（限定版）"));

	}

	static public void main(String ... args) throws IOException {
		int pass = 0;
		int fail = 0;
		ISBNUtil util = new KadokawaUtil();
		KadokawaTestManager test = new KadokawaTestManager();
		for(TestObjects obj : test.objs){
			System.out.println("## Testing ISBN = "+obj.getISBN());
			Book book = util.parseSource(obj.getISBN());
			System.out.println("#### Tested Result = "+book.getTitle());
			System.out.println("#### Excepted Result = "+obj.getExpectRes());
			if(book.getTitle().equals(obj.getExpectRes())){
				System.out.println("## Result = PASS");
				pass++;
			}else{
				System.out.println("## Result = FAIL");
				fail++;
			}

		}
		System.out.println("#");
		System.out.println("Pass = " + pass);
		System.out.println("Fail = " + fail);
	}

}
