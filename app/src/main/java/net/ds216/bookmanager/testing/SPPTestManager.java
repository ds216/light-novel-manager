package net.ds216.bookmanager.testing;

import net.ds216.bookmanager.model.Book;
import net.ds216.bookmanager.util.ISBNUtil;
import net.ds216.bookmanager.util.SPPUtil;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Marcus on 10/23/2017.
 */

public class SPPTestManager {
	private List<TestObjects> objs;

	public SPPTestManager(){
		objs = new ArrayList<>();
		objs.add(new TestObjects("9789571046570","迷茫管家與膽怯的我(04)小說"));
		objs.add(new TestObjects("9789571074528","緋彈的亞莉亞(25)"));
		objs.add(new TestObjects("9789571073521","緋彈的亞莉亞(24)"));
		objs.add(new TestObjects("9789571070933","緋彈的亞莉亞(23)"));
		objs.add(new TestObjects("9789571068442","緋彈的亞莉亞(22)"));
		objs.add(new TestObjects("9789571064680","緋彈的亞莉亞(21)"));
		objs.add(new TestObjects("9789571067261","我的朋友很少(11)"));
		objs.add(new TestObjects("4717702248628","※我的朋友很少(06)小說（特裝版）"));
		objs.add(new TestObjects("9789571052939","我的朋友很少CONNECT"));
		objs.add(new TestObjects("4717702257514","我的朋友很少Universe2【限定版】"));
		objs.add(new TestObjects("9789571051505","秒速5公分onemoreside"));
	}

	static public void main(String ... args) throws IOException {
		int pass = 0;
		int fail = 0;
		ISBNUtil util = new SPPUtil();
		SPPTestManager test = new SPPTestManager();
		for(TestObjects obj : test.objs){
			System.out.println("## Testing ISBN = "+obj.getISBN());
			Book book = util.parseSource(obj.getISBN());
			System.out.println("#### Tested Result = "+book.getTitle());
			System.out.println("#### Excepted Result = "+obj.getExpectRes());
			if(book.getTitle().equals(obj.getExpectRes())){
				System.out.println("## Result = PASS");
				pass++;
			}else{
				System.out.println("## Result = FAIL");
				fail++;
			}
			System.out.println("#");
			System.out.println("Pass = " + pass);
			System.out.println("Fail = " + fail);
		}
	}

}
