package net.ds216.bookmanager.testing;

/**
 * Created by Marcus on 10/23/2017.
 */

public class TestObjects {
	private String ISBN;
	private String ExpectRes;

	public TestObjects(String ISBN, String expectRes) {
		this.ISBN = ISBN;
		ExpectRes = expectRes;
	}

	public String getISBN() {
		return ISBN;
	}

	public void setISBN(String ISBN) {
		this.ISBN = ISBN;
	}

	public String getExpectRes() {
		return ExpectRes;
	}

	public void setExpectRes(String expectRes) {
		ExpectRes = expectRes;
	}
}
