package net.ds216.bookmanager.testing;

import net.ds216.bookmanager.model.Book;
import net.ds216.bookmanager.util.GeneralISBNUtil;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Marcus on 10/25/2017.
 */

public class GeneralTestManager {
	private List<TestObjects> objs;
	public GeneralTestManager() {
		objs = new ArrayList<>();
		objs.add(new TestObjects("9789864734948","問題兒童的最終考驗(2)"));
		objs.add(new TestObjects("4712961076289","問題兒童的最終考驗(3)失控！精靈列車！（特裝版）"));
		objs.add(new TestObjects("9789864737925","加速世界(21)"));
		objs.add(new TestObjects("9789864739073","煙花"));
		objs.add(new TestObjects("9789862874868","神的記事本(7)"));
		objs.add(new TestObjects("9789862876558","神的記事本(8)"));
		objs.add(new TestObjects("9789862872949","神的記事本(6)"));
		objs.add(new TestObjects("9789862379189","神的記事本(5)"));
		objs.add(new TestObjects("9789862375839","神的記事本(4)"));
		objs.add(new TestObjects("9789861749327","神的記事本(3)"));
		objs.add(new TestObjects("9789862875995","惡魔高校D×D(1)"));

		objs.add(new TestObjects("9789864731992","加速世界(19)"));
		objs.add(new TestObjects("9789864730360","加速世界(18)"));
		objs.add(new TestObjects("9789863259374","加速世界(15)"));
		objs.add(new TestObjects("9789863256991","加速世界(14)"));
		objs.add(new TestObjects("9789863255369","加速世界(13)"));
		objs.add(new TestObjects("9789863252375","加速世界(12)"));
		objs.add(new TestObjects("9789863250234","加速世界(11)"));
		objs.add(new TestObjects("9789864732319","問題兒童的最終考驗(1)"));
		objs.add(new TestObjects("9789863664840","神的記事本(9)（完）"));
		objs.add(new TestObjects("9789864734566","你的名字AnotherSide:Earthbound"));
		objs.add(new TestObjects("9789862875919","問題兒童都來自異世界？(1)"));
		objs.add(new TestObjects("9789863669043","問題兒童都來自異世界?(12)（完）"));
		objs.add(new TestObjects("9789863660347","問題兒童都來自異世界？(9)(普通版)"));
		objs.add(new TestObjects("4712961070393","問題兒童都來自異世界?(12)（完）（限定版）"));
		objs.add(new TestObjects("9789571046570","迷茫管家與膽怯的我(04)小說"));
		objs.add(new TestObjects("9789571074528","緋彈的亞莉亞(25)"));
		objs.add(new TestObjects("9789571073521","緋彈的亞莉亞(24)"));
		objs.add(new TestObjects("9789571070933","緋彈的亞莉亞(23)"));
		objs.add(new TestObjects("9789571068442","緋彈的亞莉亞(22)"));
		objs.add(new TestObjects("9789571064680","緋彈的亞莉亞(21)"));
		objs.add(new TestObjects("9789571067261","我的朋友很少(11)"));
		objs.add(new TestObjects("4717702248628","※我的朋友很少(06)小說（特裝版）"));
		objs.add(new TestObjects("9789571052939","我的朋友很少CONNECT"));
		objs.add(new TestObjects("4717702257514","我的朋友很少Universe2【限定版】"));
		objs.add(new TestObjects("9789571051505","秒速5公分onemoreside"));
	}

	static public void main(String ... args) throws IOException {
		int pass = 0;
		int fail = 0;
		GeneralISBNUtil util = new GeneralISBNUtil();
		GeneralTestManager test = new GeneralTestManager();
		for(TestObjects obj : test.objs){
			System.out.println("## Testing ISBN = "+obj.getISBN());
			Book book = util.forBook(obj.getISBN());
			System.out.println("#### Tested Result = "+book.getTitle());
			System.out.println("#### Excepted Result = "+obj.getExpectRes());
			if(book.getTitle().equals(obj.getExpectRes())){
				System.out.println("## Result = PASS");
				pass++;
			}else{
				System.out.println("## Result = FAIL");
				fail++;
			}
			System.out.println("#");
			System.out.println("Pass = " + pass);
			System.out.println("Fail = " + fail);
		}
	}
}
