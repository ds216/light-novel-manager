package net.ds216.bookmanager.model;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName="bookSeries")
public class BookSeries {
	@PrimaryKey
	@NonNull
	@ColumnInfo(name="id")
	private String id;
	@ColumnInfo(name="title")
	private String title;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
}
