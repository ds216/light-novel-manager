package net.ds216.bookmanager.model;

import java.util.List;

import androidx.room.Embedded;
import androidx.room.Relation;

public class SeriesAndBooks {
	@Embedded
	public BookSeries series;
	@Relation(parentColumn = "id",
			entityColumn = "seriesId")
	public List<Book> book;
}
