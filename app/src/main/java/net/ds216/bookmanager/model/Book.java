package net.ds216.bookmanager.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

/**
 * Created by Marcus on 10/23/2017.
 */
@Entity(foreignKeys = @ForeignKey(entity = BookSeries.class,
		parentColumns = "id",
		childColumns = "seriesId"),
		tableName="book")
public class Book {
	@PrimaryKey
	@ColumnInfo(name="id")
	@NonNull
	private String id;
	@ColumnInfo(name="title")
	private String title;
	@ColumnInfo(name="volume")
	private String volume;
	@ColumnInfo(name="isbn")
	private String ISBN;
	@ColumnInfo(name="seriesId")
	private int seriesId;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getVolume() {
		return volume;
	}

	public void setVolume(String volume) {
		this.volume = volume;
	}

	public String getISBN() {
		return ISBN;
	}

	public void setISBN(String ISBN) {
		this.ISBN = ISBN;
	}

	public int getSeriesId() {
		return seriesId;
	}

	public void setSeriesId(int seriesId) {
		this.seriesId = seriesId;
	}

	@Override
	public String toString() {
		return "Book{" +
				"id='" + id + '\'' +
				", title='" + title + '\'' +
				", volume='" + volume + '\'' +
				", ISBN='" + ISBN + '\'' +
				'}';
	}
}
