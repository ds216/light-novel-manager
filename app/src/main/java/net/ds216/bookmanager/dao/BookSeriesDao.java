package net.ds216.bookmanager.dao;

import net.ds216.bookmanager.model.BookSeries;
import net.ds216.bookmanager.model.SeriesAndBooks;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

@Dao
public interface BookSeriesDao {

	@Query("SELECT * FROM bookSeries")
	List<BookSeries> getAll();

	@Query("SELECT * FROM bookSeries")
	List<SeriesAndBooks> getAllSeriesAndBook();

	@Query("SELECT * FROM bookSeries WHERE id IN (:ids)")
	List<BookSeries> getByIds(int[] ids);

	@Insert
	void insertAll(BookSeries ... bookSeries);

	@Delete
	void delete(BookSeries bookSeries);
}
