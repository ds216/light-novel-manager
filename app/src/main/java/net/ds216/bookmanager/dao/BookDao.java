package net.ds216.bookmanager.dao;

import net.ds216.bookmanager.model.Book;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

@Dao
public interface BookDao {
	@Query("SELECT * FROM book")
	List<Book> getAll();

	@Query("SELECT * FROM book WHERE id IN (:ids)")
	List<Book> getByIds(int[] ids);

	@Insert
	void insertAll(Book ... books);

	@Delete
	void delete(Book book);
}
