package net.ds216.bookmanager.util;

import net.ds216.bookmanager.dao.BookDao;
import net.ds216.bookmanager.dao.BookSeriesDao;
import net.ds216.bookmanager.model.Book;
import net.ds216.bookmanager.model.BookSeries;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {Book.class, BookSeries.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {

	public abstract BookDao bookDao();
	public abstract BookSeriesDao bookSeriesDao();

}
