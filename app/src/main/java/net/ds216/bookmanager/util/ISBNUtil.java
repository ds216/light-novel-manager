package net.ds216.bookmanager.util;

import net.ds216.bookmanager.model.Book;

import java.io.IOException;

/**
 * Created by Marcus on 10/25/2017.
 */

public interface ISBNUtil {

	String getSource(String ISBN) throws IOException;
	Book parseSource(String ISBN) throws IOException;

}
