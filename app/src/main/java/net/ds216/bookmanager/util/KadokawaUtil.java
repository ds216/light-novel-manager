package net.ds216.bookmanager.util;

import net.ds216.bookmanager.model.Book;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.security.cert.Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;


/**
 * Created by Marcus on 10/24/2017.
 */

public class KadokawaUtil  implements ISBNUtil {

	private final static String URL_BASE = "https://www.kadokawa.com.tw/search_list.htm?Keyword=";
	private final String REGEX = "<h3class=\"title\">(.*)<\\/h3>.*<pclass=\"text\">出版/上市日期：(.*)<\\/p><p";
	private final String VOLREGEX = "\\((.*)\\)";


	@Override
	public String getSource(String ISBN) throws IOException {
		String res="";
		boolean start= false, end = false;
		BufferedReader in = null;
		URL url = new URL(URL_BASE+ISBN);
		HttpsURLConnection connection = null;
		try {
			TrustManager[] trustMyCerts = new TrustManager[]{new X509TrustManager() {
				@Override
				public void checkClientTrusted(X509Certificate[] chain,
											   String authType) throws CertificateException {
				}

				@Override
				public void checkServerTrusted(X509Certificate[] chain,
											   String authType) throws CertificateException {
				}

				@Override
				public X509Certificate[] getAcceptedIssuers() {
					return null;
				}
			}};
			HostnameVerifier hv = new HostnameVerifier() {
				public boolean verify(String urlHostName, SSLSession session) {
					System.out.println("Warning: URL Host: "+urlHostName+" vs. "+session.getPeerHost());
					return urlHostName.equalsIgnoreCase(session.getPeerHost());
				}
			};

			//Initial SSLContext
			SSLContext sc = SSLContext.getInstance("SSL");
			sc.init(null, trustMyCerts, new java.security.SecureRandom());

			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
			HttpsURLConnection.setDefaultHostnameVerifier(hv);
			connection = (HttpsURLConnection)url.openConnection();
			connection.setRequestMethod("GET");
			connection.setRequestProperty("Host","www.kadokawa.com.tw");
			connection.setRequestProperty("User-Agent","Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36");
			connection.setRequestProperty("Accept","text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
			connection.setRequestProperty("Referer","https://www.kadokawa.com.tw/p0-search.php?S_Item=1&Keywords=9789864734948");
			connection.setRequestProperty("Cookie","PHPSESSID=pv83f5fi26epk62m9ulcps6cf2; incap_ses_627_1045467=ALsXAbkO6lHneD5zj42zCNRc71kAAAAAXUnAQ/BwqWsjQCM/fCrG6A==; visid_incap_1045467=vpDvgFXyRvKEqMp1T9OTwjiE21kAAAAAQkIPAAAAAACABbl/AbezsYriRLL4vfXTSlQqLii5e0VK; _gat=1; _ga=GA1.3.219376138.1507558457; _gid=GA1.3.1543795968.1508857641");
			connection.setDoOutput(true);
			connection.connect();

			connection.getOutputStream().flush();
//			System.out.println("Response Code : " + connection.getResponseCode());
//			System.out.println("Cipher Suite : " + connection.getCipherSuite());
//			System.out.println("\n");

			Certificate[] certs = connection.getServerCertificates();
			for(Certificate cert : certs){
//				System.out.println("Cert Type : " + cert.getType());
//				System.out.println("Cert Hash Code : " + cert.hashCode());
//				System.out.println("Cert Public Key Algorithm : " + cert.getPublicKey().getAlgorithm());
//				System.out.println("Cert Public Key Format : " + cert.getPublicKey().getFormat());
//				System.out.println("\n");
			}
			InputStream ins = connection.getInputStream();
			InputStreamReader isr = new InputStreamReader(ins);
			in = new BufferedReader(isr);
//
//			String inputLine;
//
//			while ((inputLine = in.readLine()) != null)
//			{
//				System.out.println(inputLine);
//			}
//			in.close();
		}catch(Exception e){
			e.printStackTrace();
		}


		String line;
		while ((line = in.readLine()) != null) {
			if(line.contains("incapsula.com")){
				System.out.print("Please update cookies!!");
				return null;
			}
			if(line.contains("<div class=\"search_listTitle\">")){
				start = true;
			}
			if(line.contains("</div><!-- /.pro clearfix -->")){
				end = true;
			}
		//	System.out.println(line);
			if(start && !end) {
				res = res + line;
			}
		}
		connection.disconnect();
		return res;
	}

	@Override
	public Book parseSource(String ISBN) throws IOException {
		String source = getSource(ISBN).replaceAll("\\s","");
		Pattern pattern = Pattern.compile(REGEX);
		Matcher matcher = pattern.matcher(source);
		System.out.println(source);
		if(matcher.find()){
			String bookname = matcher.group(1);
		//	String date =  matcher.group(2);
			Book newBook = new Book();
			newBook.setISBN(ISBN);
			newBook.setTitle(bookname);
			Pattern Volpattern = Pattern.compile(VOLREGEX);
			Matcher Volmatcher = Volpattern.matcher(bookname);
			if(Volmatcher.find()) {
				newBook.setVolume(Volmatcher.group(1));
			}
			return newBook;
		}else{
			return null;
		}	}

	public static void main(String ... args) throws Exception{
		KadokawaUtil util = new KadokawaUtil();
		System.out.println(util.parseSource("9789864734948"));
		System.out.println(util.parseSource("4712961076289"));
		System.out.println(util.parseSource("9789864737925"));
		System.out.println(util.parseSource("9789864739073"));
		System.out.println(util.parseSource("9789862874868"));
		System.out.println(util.parseSource("9789862876558"));
		System.out.println(util.parseSource("9789862872949"));
		System.out.println(util.parseSource("9789862379189"));
		System.out.println(util.parseSource("9789862375839"));
		System.out.println(util.parseSource("9789861749327"));
		System.out.println(util.parseSource("9789862875995"));
		System.out.println(util.parseSource("9789864731992"));
		System.out.println(util.parseSource("9789864730360"));
		System.out.println(util.parseSource("9789863259374"));
		System.out.println(util.parseSource("9789863256991"));
		System.out.println(util.parseSource("9789863255369"));
		System.out.println(util.parseSource("9789863252375"));
		System.out.println(util.parseSource("9789863250234"));
		System.out.println(util.parseSource("9789864732319"));
		System.out.println(util.parseSource("9789863664840"));
		System.out.println(util.parseSource("9789864734566"));
		System.out.println(util.parseSource("9789862875919"));
		System.out.println(util.parseSource("9789863669043"));
		System.out.println(util.parseSource("9789863660347"));
		System.out.println(util.parseSource("4712961070393"));
	}

}
