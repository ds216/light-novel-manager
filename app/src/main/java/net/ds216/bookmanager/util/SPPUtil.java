package net.ds216.bookmanager.util;

import net.ds216.bookmanager.model.Book;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Marcus on 10/22/2017.
 */

public class SPPUtil implements ISBNUtil{

	private final static String URL_BASE = "http://www.spp.com.tw/spp2006/all/asp/searchisbn.asp?vch=&vkey=";
	private final String REGEX = "<ahref=\".*\"><h3class=\"colorbrowntitle.*\">(.*)<\\/h3>.*<br>出版日期：(.*)<br>書";
	private final String VOLREGEX = "\\((.*)\\)";

	public String getSource(String ISBN) throws IOException {
		String res="";
		boolean start= false, end = false;
		BufferedReader in = null;
		URL url = new URL(URL_BASE+ISBN);
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setDoOutput(true);
		connection.connect();
		connection.getOutputStream().flush();
		in = new BufferedReader(new InputStreamReader(connection
				.getInputStream(), "UTF-8"));

		String line;
		while ((line = in.readLine()) != null) {
			if(line.contains("<ul id=\"infinite_scroll\"")){
				start = true;
			}
			if(line.contains("</ul><!--infinite_scroll-->")){
				end = true;
			}
			//System.out.println(line);
			if(start && !end) {
				res = res + line;
			}
		}
		connection.disconnect();
		return res;
	}

	public Book parseSource(String ISBN) throws IOException {
		String source = getSource(ISBN).replaceAll("\\s","");
		Pattern pattern = Pattern.compile(REGEX);
		Matcher matcher = pattern.matcher(source);
	//	System.out.println(source);
		if(matcher.find()){
			String bookname = matcher.group(1);
			String date =  matcher.group(2);
			Book newBook = new Book();
			newBook.setISBN(ISBN);
			newBook.setTitle(bookname);
			Pattern Volpattern = Pattern.compile(VOLREGEX);
			Matcher Volmatcher = Volpattern.matcher(bookname);
			if(Volmatcher.find()) {
				newBook.setVolume(Volmatcher.group(1));
			}
			return newBook;
		}else{
			return null;
		}
	}

	public static void main(String ... args) throws Exception{
		SPPUtil util = new SPPUtil();
		System.out.println(util.parseSource("9789571046570"));
	}

}
