package net.ds216.bookmanager.util;

import net.ds216.bookmanager.model.Book;

/**
 * Created by Marcus on 10/25/2017.
 */

public class GeneralISBNUtil {

	public Book forBook(String ISBN){
		ISBNUtil util = new KadokawaUtil();
		Book res = null;
		try {
			res = util.parseSource(ISBN);
			if (res != null) {
				return res;
			}
			util = new SPPUtil();
			res = util.parseSource(ISBN);
			if (res != null) {
				return res;
			}
		}catch(Exception e){
			e.printStackTrace();;
			return null;
		}
		return null;
	}

}
